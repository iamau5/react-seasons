import React from 'react';
import ReactDOM from 'react-dom';
import SeasonDisplay from './SeasonDisplay';
import Spinner from './Spinner';
import './border.css';

class App extends React.Component {
    
    /*constructor(props) {
        super(props);

        // THIS IS THE ONLY TIME we so direct assignment to this state.
        this.state = { lat: null, errorMsg: '' };
    }*/

    state = { lat: null, errorMsg: '' };

    componentDidMount() 
    {
        console.log('My component was rendered to the screen');
        
        window.navigator.geolocation.getCurrentPosition(
            (position) => {
                // we called setState!
                this.setState({ lat: position.coords.latitude })

                // We did not!!
                // this.state.lat = position.coords.latitude
            },
            (err) => this.setState({ errorMsg: err.message })
        );
    }

    componentDidUpdate() 
    {
        console.log('My component was just updated to the screen - it rerendered!!');
    }

    renderContent()
    {
        if (this.state.errorMsg && !this.state.lat)
            return <div> Error : {this.state.errorMsg}</div>;
        
        if(!this.state.errorMsg && this.state.lat)
            return <SeasonDisplay lat={this.state.lat} />

        return <Spinner message="Please accept request" />
    }

    // React says we have to  define render!
    render() {
        return (
            <div className="border-line">
                {this.renderContent()}
            </div>
        );
    }
}

ReactDOM.render(<App />, document.querySelector('#root'));